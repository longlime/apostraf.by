(function( window ) {
	var
		myNavigator = navigator.userAgent.toLowerCase(),
		version = (myNavigator.indexOf('msie') != -1 ? parseInt(myNavigator.split('msie')[1]) : false);

	// if ( version < 10 && version ) {
	// 	return false;
	// }

	var
		document = window.document,
		location = window.location,
		body = document.body,
		screenElem = document.compatMode == "CSS1Compat" ? document.documentElement : document.body;

	var
		getElemsBySelector = document.querySelectorAll.bind( document ),

		$ = function( selector ) {
			return ( typeof selector === "string" ) ? getElemsBySelector( selector ) : [ selector ];
		},

		eachElems = function( obj, iterator ) {
			for ( var i = 0, l = obj.length; i < l; i++ ) {
				if ( iterator.call( obj[ i ], obj[ i ], i ) === false ) break;
			}
		},

		_addEventListener = function( elem, evnt, fn ) {
			if ( elem.addEventListener ) {
				elem.addEventListener( evnt, fn, false );
			} else if ( elem.attachEvent ) {
				elem.attachEvent( "on" + evnt, fn );
			}
		},

		addEvent = function( selector, evnt, fn ) {
			eachElems( $(selector), function( elem ) {
				_addEventListener( elem, evnt, fn );
			});
		},

		animate = function( step, opts ) {
			var timer,
				start = new Date,
				opts = {
					duration: 300,
					delay: 10,
					delta: function( x ) {
						return Math.sin( x * Math.PI / 2 );
					},
					done: function() {}
				},

			timer = setInterval(function() {
				var progress = ( new Date - start ) / opts.duration;

				if ( progress > 1 ) {
					progress = 1;
				}

				step( opts.delta( progress ) );

				if ( progress === 1 ) {
					clearInterval( timer );
					opts.done.call( this );
				}

			}, opts.delay );
		},

		_trim = function( str ) {
			return str.replace(/^\s+|\s+$/, "");
		},

		_hasClass = function( elem, className ) {
			return elem.className.indexOf(className) === -1 ? false : true;
		},

		_addClass = function( elem, className ) {
			if (elem.classList) {
				elem.classList.add(className);
			} else {
				var currentList = elem.className;
				currentList = currentList.split(" ");
				currentList.push(className);
				elem.className = _trim(currentList.join(" ")).replace(/(\s\s)*/, "");
			}
		},

		_removeClass = function( elem, className ) {
			if (elem.classList) {
				elem.classList.remove(className);
			} else {
				var currentList = elem.className;
				currentList = currentList.split(" ");
				var index = currentList.indexOf(className);
				if ( index !== -1 ) {
					currentList.splice(index, 1);
				}
				elem.className = _trim(currentList.join(" ")).replace(/(\s\s)*/, "");
			}
		},

		_dataset = function( elem, name ) {
			return elem.dataset ? elem.dataset[name] : elem.getAttribute("data-"+name);
		};

	var _setBackground = function() {
		var bgImg = $(".background IMG")[ 0 ];

		var bgImgWidth = parseInt( bgImg.getAttribute("data-width") );
		var bgImgHeight = parseInt( bgImg.getAttribute("data-height") );

		var setSize = function( width, height ) {
			bgImg.style.width = (( typeof width === "string" ) ? width : width) + "px";
			bgImg.style.height = (( typeof height === "string" ) ? height : height) + "px";
		};

		var changeSize = function() {
			var screenWidth = screenElem.clientWidth;
			var screenHeight = screenElem.clientHeight;

			if ( screenWidth / bgImgWidth > screenHeight / bgImgHeight ) {
				setSize( screenWidth, "auto" );
			} else {
				setSize( "auto", screenHeight );
			}
		};

		changeSize();

		addEvent( window, "resize", changeSize );
	};

	var App = (function() {
		function App() {
			this.init();
		}

		App.prototype.init = function() {
			var obj = this;

			for ( var name in obj ) {
				if ( name !== "init" && !/^_/.test( name ) ) obj[ name ].call( obj );
			}
		};

		return App;
	})();

	//
	App.prototype.setBackground = _setBackground;

	//
	App.prototype._addHandlersInnerScreen = function() {
		var that = this;

		addEvent( ".screen-eq0", "click", function() {
			// body.classList.add("show-eq1");
			that._openInnerScreen(true);
		});
		// addEvent( ".screen-eq0", "transitionend", function( transition ) {
		// 	if ( transition.propertyName === "margin-left" ) {
		// 		this.style.display = "none";
		// 		location.hash = "content";
		// 	}
		// });

		var checkLocation = function() {
			if ( location.hash === "#content" ) {
				that._openInnerScreen(false);
				// _addClass( body, "noStartAnimation" );
				// body.classList.add( "noStartAnimation" );
			} else {
				// body.classList.remove( "noStartAnimation" );
				// body.classList.remove( "show-eq1" );
				_removeClass( body, "noStartAnimation" );
				_removeClass( body, "show-eq1" );
			}
		};

		checkLocation();

		_addEventListener( window, "hashchange", checkLocation );
	};

	//
	App.prototype._sectionsHeight = function() {
		eachElems( $(".block"), function( elem ) {
			var height = elem.querySelector(".block-content").offsetHeight;
			var screenHeight = screenElem.clientHeight;
			elem.querySelector(".div_tr").style.height = ( screenHeight > height ? screenHeight : height ) + "px";
		});
	};

	App.prototype._scrolling = function() {
		var container = $(".screen-eq1")[0];
		var dh = $(".blocks")[0];
		var track = $(".scroll")[0];
		var trackHandler = track.children[0];
		var deltaHeight = dh.offsetHeight - screenElem.offsetHeight;

		container.style.overflow = "hidden";

		var blocks = container.children[0].children;
		var pos0 = blocks[0].offsetTop;
		var pos1 = blocks[1].offsetTop;
		var pos2 = blocks[2].offsetTop;
		var pos3 = blocks[3].offsetTop;
		var pos = [pos0, pos1, pos2, pos3];
		var navLinks = $(".sidebar-navigation")[0].children;
		var prevId = 0;

		var checkNav = function( value ) {
			var id = 0;
			var pos = [blocks[0].offsetTop, blocks[1].offsetTop, blocks[2].offsetTop, blocks[3].offsetTop];
			if ( value >= pos[3] ) {
				id = 3;
			} else if ( value >= pos[2] ) {
				id = 2;
			} else if ( value >= pos[1] ) {
				id = 1;
			} else {
				id = 0;
			}

			if ( id !== prevId ) {
				prevId = id;

				for (var i = 0, l = navLinks.length; i < l; i++) {
					if (i == id) {
						_addClass( navLinks[i], "sidebar-link--active" );
					} else {
						_removeClass( navLinks[i], "sidebar-link--active");
					}
				}
			}
		};

		var scrollTo = function( id ) {
			id = parseInt(id.substr(1));
			var pos = [blocks[0].offsetTop, blocks[1].offsetTop, blocks[2].offsetTop, blocks[3].offsetTop];
			var value = pos[id];
			var current = container.scrollTop;

			animate(function( x ) {
				container.scrollTop = current * (1 - x) + value * x;
				var trackValue = container.scrollTop * ( (track.offsetHeight - trackHandler.offsetHeight) / (dh.offsetHeight - screenElem.clientHeight) );
				trackHandler.style.top = (trackValue) + "px";
			});

			checkNav(value);
		};

		checkNav(0);

		var scrollWithWheel = function( value ) {
			var current = container.scrollTop;

			container.scrollTop = current - value;

			var trackValue = container.scrollTop * ( (track.offsetHeight - trackHandler.offsetHeight) / (dh.offsetHeight - screenElem.clientHeight) );
			trackHandler.style.top = trackValue + "px";

			checkNav( container.scrollTop );
		};

		var scrollWithTrack = function( value ) {
			var scrollTop = value / ( (track.offsetHeight - trackHandler.offsetHeight) / (dh.offsetHeight - screenElem.clientHeight) );

			trackHandler.style.top = value + "px";
			container.scrollTop = scrollTop;

			checkNav( scrollTop );
		};

		var wheel = function(e) {
			var e = window.event || e;

			e.preventDefault();
			e.stopPropagation();

			var y = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
			// var y = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail))) * 80;

			scrollWithWheel( y * 50 );

			return false;
		};

		var offsetTop = track.offsetTop;
		var delta;

		var moving = function( e ) {
			e = e || event;

			var y = e.y || e.clientY;

			var value = y - offsetTop - delta;

			if ( value < 0 )
				value = 0;

			if ( value > track.offsetHeight - trackHandler.offsetHeight )
				value = track.offsetHeight - trackHandler.offsetHeight;

			scrollWithTrack( value );
		};

		var startDrag = function( e ) {
			delta = ( e.y || e.clientY ) - offsetTop - trackHandler.offsetTop;
			_addEventListener( body, "mousemove", moving );
			_addClass( body, "noselecting" );
			// body.classList.add( "noselecting" );
		};

		var stopDrag = function() {
			body.removeEventListener( "mousemove", moving );
			_removeClass( body, "noselecting" );
			// body.classList.remove( "noselecting" );
		};

		_addEventListener( trackHandler, "mousedown", startDrag );
		_addEventListener( body, "mouseup", stopDrag );
		if (container.addEventListener) {
		  if ('onwheel' in document) {
		    // IE9+, FF17+
		    container.addEventListener ("wheel", onWheel, false);
		  } else if ('onmousewheel' in document) {
		    // устаревший вариант события
		    container.addEventListener ("mousewheel", onWheel, false);
		  } else {
		    // 3.5 <= Firefox < 17, более старое событие DOMMouseScroll пропустим
		    container.addEventListener ("MozMousePixelScroll", onWheel, false);
		  }
		} else { // IE<9
		  container.attachEvent ("onmousewheel", onWheel);
		}

		function onWheel(e) {
			e = e || window.event;

			// wheelDelta не дает возможность узнать количество пикселей
			var delta = -e.deltaY || -e.detail || e.wheelDelta;

			scrollWithWheel( delta );

			e.preventDefault ? e.preventDefault() : (e.returnValue = false);
		}

		var clicked = function( e ) {
			e.preventDefault();
			e.stopPropagation();

			// if (!body.classList.contains("openedGalery"))
			if (!_hasClass(body, "openedGalery"))
				scrollTo( this.hash );
		};

		eachElems( navLinks, function( elem, index ) {
			_addEventListener( elem, "click", clicked );
		});

		scrollTo("#0");
	};

	//
	App.prototype.makeContentAppearance = function() {
		var that = this;

		that._sectionsHeight();
		that._scrolling();
	};

	//
	App.prototype.addMainHandlers = function() {
		var that = this;

		that._addHandlersInnerScreen();

		var eventResize = function() {
			that._sectionsHeight();
		};

		_addEventListener( window, "resize", eventResize );
	};

	//
	App.prototype._openInnerScreen = function(isAnimated) {
		if ( isAnimated ) {
			_addClass( body, "show-eq1" );
			setTimeout(function() {
				$(".screen-eq0")[0].style.display = "none";
				location.hash = "content";
			}, 300);
		} else {
			_addClass( body, "noStartAnimation" );
		}
	};

	App.prototype._openWork = function( id, count, text, prefix ) {
		var bigImage = "<img src='"+prefix+"images/work/" + id + "_x_01.jpg'>";
		var smallImages = "";

		for ( var i = 1, l = parseInt(count); i <= l; i++ ) {
			var sid = i > 9 ? i : "0" + i;
			smallImages += "<img " + (i === 1 ? "class='active'" : "") + " src='"+prefix+"images/work/" + id + "_s_" + sid + ".jpg'>";
		}

		_addClass( body, "openedGalery" );
		// body.classList.add("openedGalery");

		App.galery.textBlock.innerHTML = text;
		App.galery.bigImage.innerHTML = bigImage;
		App.galery.smallImages.innerHTML = smallImages;
	};

	App.prototype._hideWork = function() {
		// body.classList.add("closingGalery");
		_addClass( body, "closingGalery" );
		// body.classList.remove("openedGalery");
		_removeClass( body, "openedGalery" );
		setTimeout(function() {
			// body.classList.remove("closingGalery");
			_removeClass( body, "closingGalery" );
		}, 200);
	};

	App.prototype.works = function() {
		var bgalery = $(".galery_content")[0];

		App.galery = {
			bigImage: bgalery.querySelector(".galery_bigImage"),
			smallImages: bgalery.querySelector(".galery_smallImages"),
			closeButton: bgalery.querySelector(".galery-buttonClose"),
			textBlock: $(".sidebar-content--galery")[0]
		}

		var prefix = $(".block_works .works-item FIGURE IMG")[0].getAttribute("src").replace(/^(.*)images.+$/, "$1");
		var that = this;
		var clicked = function() {
			// var data = this.dataset;
			var text = this.querySelector(".works-item-text").innerHTML;
			that._openWork( _dataset(this, "id"), _dataset(this, "count"), text, prefix );
		};

		var keypressed = function( e ) {
			if ( (e.which || e.keyCode) === 27 ) {
				that._hideWork();
				return false;
			}
		};

		var clickOnSmall = function(e) {
			var target = e.target;
			if (target.tagName.toLowerCase() === "img") {
				var src = target.getAttribute("src");
				var num = src.replace(/.+(\d{2})\.\w+$/, "$1");
				var id = src.replace(/.+(\d{3})_s_\d{2}\.\w+$/, "$1");

				var smallImgs = App.galery.smallImages.children;

				for ( var i = 0, l = smallImgs.length; i < l; i++ ) {
					// smallImgs[i].classList.remove("active");
					_removeClass( smallImgs[i], "active" );
				}

				// smallImgs[parseInt(num) - 1].classList.add("active");
				_addClass(smallImgs[parseInt(num) - 1], "active");

				App.galery.bigImage.children[0].src = (prefix+"images/work/" + id + "_x_" + num + ".jpg");
			}
		};

		addEvent(".works-item", "click", clicked);
		_addEventListener( App.galery.closeButton, "click", that._hideWork );
		_addEventListener( App.galery.smallImages, "click", clickOnSmall );
		_addEventListener( window, "keyup", keypressed );
	};

	var apostraf = new App();

})( window );
