(function (e) {
    var t = e.document,
        n = t.getElementById("screen-bg"),
        r = n.getElementsByTagName("img")[0],
        screenElem = document.compatMode == "CSS1Compat" ? document.documentElement : document.body,
        screenWidth = screenElem.clientWidth,
		screenHeight = screenElem.clientHeight,
        i = function (e, t, n) {
            return e["on" + t] = n
        },
        s = function (e, t) {
            return e.getAttribute("data-" + t)
        },
        setSize = function( width, height ) {
        	r.style.width = width;
        	r.style.height = height;
        },
        o = function () {
            var t = r,
                n = parseInt(s(t, "width")),
                i = parseInt(s(t, "height"));
            if ( screenWidth / n > screenHeight / i ) {
            	setSize( screenWidth + "px", "auto" );
            } else {
            	setSize( "auto", screenHeight + "px" );
            }
        },
        u = function () {
            e.onresize = function () {
                o()
            }, o()
        },
        a = t.getElementById("galery"),
        f = function () {
            a.className = "showed"
        },
        l = function () {
            a.className = ""
        },
        c = function () {
            i(t.getElementById("start-page"), "click", f), i(t.getElementById("galery-buttonClose"), "click", l)
        };
    c(), u()
})(window);
